# Introdução 

Agradeco por ter me considerado para a vaga.

Aplicativo de Dribbble que consome a API Rest de Shots. Como foi solicitado que testar as habilidades em Objective-C e Swift, escrevi o aplicativo misto em ambas as linguagens. Toda a parte de controlador foi escrito em Objective-C enquanto o modelo foi escrito em Swift.

Aplicativo utiliza como gerenciador de dependências o CocoaPods, apesar de estar incluindo as libs no rep, caso nao consiga rodar o projeto, talvez seja necessario rodar o pod install.

# Pre-requisitos implementados
  - Arquivo .gitignore
  - Usar Storyboard e Autolayout
  - Gestão de dependências no projeto com Cocoapods
  - Framework para Comunicação com API com o AFNetwork
  - Mapeamento json -> Objeto utilizando o SwiftyJSON
  - Lista de shots API (http://developer.dribbble.com/v1/shots/)
  - Paginação automática (scroll infinito) na tela de lista de shots
  - Paginação deve detectar quando chega a última página e parar de solicitar mais
  - Pull to refresh
  - Tela de detalhe de um shot ao clicar em um item da lista de shots
  - Tela de detalhe de um shot deve conter nome do autor, foto e descrição do shot
  - Testes unitários no projeto utilizando XCTests somente da classe Shot 
  - App Universal: roda bem no iPad e iPhone em portrait e landscape.
  - Cache de Imagens com o SDWebImage
  - Compartilhar shots no facebook e twitter utilizando o UIActivityController
