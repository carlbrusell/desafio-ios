//
//  User.swift
//  Dribbble
//
//  Created by Carl Osorio on 07/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    
    @objc var id: String?
    @objc var name: String?
    @objc var username: String?
    @objc var location: String?
    @objc var avatar: String?
    @objc var bio: String?
    @objc var profileLink: String?
    
    init(dic: JSON) {
        
        id = dic["id"].string
        name = dic["name"].string
        username = dic["username"].string
        location = dic["location"].string
        avatar = dic["avatar_url"].string
        bio = dic["bio"].string
        profileLink = dic["html_url"].string
        
    }

}
