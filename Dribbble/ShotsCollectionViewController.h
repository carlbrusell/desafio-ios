//
//  ViewController.h
//  Dribbble
//
//  Created by Carl Osorio on 07/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dribbble-Swift.h"

@interface ShotsCollectionViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

@property NSMutableArray<Shot *> *shots;

@property int actualPage;
@property BOOL itsOver;

@end

