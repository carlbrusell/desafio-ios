//
//  ShotCollectionViewCell.m
//  Dribbble
//
//  Created by Carl Osorio on 07/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

#import "ShotCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation ShotCollectionViewCell

-(void)awakeFromNib {
    [super awakeFromNib];
    _image.superview.layer.borderWidth = 1.0f;
    _image.superview.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _image.superview.layer.cornerRadius = 4.0f;
}

@end
