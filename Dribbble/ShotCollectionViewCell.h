//
//  ShotCollectionViewCell.h
//  Dribbble
//
//  Created by Carl Osorio on 07/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShotCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
