//
//  ShotTableViewController.h
//  Dribbble
//
//  Created by Carl Osorio on 08/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dribbble-Swift.h"

@interface ShotTableViewController : UITableViewController

@property Shot *shot;

@property (weak, nonatomic) IBOutlet UIImageView *authorImage;

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *shotTitle;
@property (weak, nonatomic) IBOutlet UILabel *author;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *views;
@property (weak, nonatomic) IBOutlet UILabel *comments;
@property (weak, nonatomic) IBOutlet UILabel *likes;
@property (weak, nonatomic) IBOutlet UILabel *descript;
@property (weak, nonatomic) IBOutlet UILabel *tags;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;

@end
