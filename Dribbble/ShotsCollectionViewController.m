//
//  ViewController.m
//  Dribbble
//
//  Created by Carl Osorio on 07/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

#import "ShotsCollectionViewController.h"
#import "ShotCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ShotTableViewController.h"
#import <UIScrollView_InfiniteScroll/UIScrollView+InfiniteScroll.h>

@interface ShotsCollectionViewController ()

@end

@implementation ShotsCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _shots = [[NSMutableArray alloc] init];
    _actualPage = 1;
    _itsOver = NO;
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    [refresh addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refresh];
    
    [DribbbleService getShotsWithPage: 0 completion:^(BOOL success, NSString *error, NSArray<Shot *> * _Nullable shots) {
        if(success) {
            self.shots = shots.mutableCopy;
            _actualPage += 1;
            [self.collectionView reloadData];
        } else {
            [self errorMessage: error];
        }
        
    }];
    
}

-(void) refresh: (UIRefreshControl *)refreshControl {
    
    _actualPage = 0;
    _itsOver = NO;
    
    [DribbbleService getShotsWithPage: _actualPage completion:^(BOOL success, NSString *error, NSArray<Shot *> * _Nullable shots) {
        if(success) {
            self.shots = shots.mutableCopy;
            _actualPage += 1;
            [self.collectionView reloadData];
            [refreshControl endRefreshing];
        } else {
            [self errorMessage: error];
        }
        
    }];
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.shots.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if([UIDevice.currentDevice.model isEqualToString: @"iPhone"]) {
        return CGSizeMake(self.view.frame.size.width / 2 - 11, self.view.frame.size.width / 3);
    } else {
        return CGSizeMake(self.view.frame.size.width / 4 - 11, self.view.frame.size.width / 4 - 11);
    }
    
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(2, 4, 2, 4);
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == self.shots.count - 1 && !_itsOver) {
        
        int requestedPage = _actualPage;
        [self.collectionView beginInfiniteScroll: true];
        
        [DribbbleService getShotsWithPage: _actualPage completion:^(BOOL success, NSString *error, NSArray<Shot *> * _Nullable shots) {
            
            if(success && _actualPage <= requestedPage) {
                
                [self.shots addObjectsFromArray: shots];
                _actualPage += 1;
                
                [self.collectionView performBatchUpdates:^{
                    
                    NSMutableArray *indexPathesToBeInserted = [[NSMutableArray alloc] init];
                    
                    for (Shot *shot in shots) {
                        
                        NSIndexPath *i = [NSIndexPath indexPathForRow: [_shots indexOfObject:shot] inSection:0];
                        [indexPathesToBeInserted addObject:i];
                        
                    }
                    
                    [self.collectionView insertItemsAtIndexPaths:indexPathesToBeInserted];
                    
                } completion:^(BOOL finished) {
                    
                    [self.collectionView finishInfiniteScroll];
                    
                }];
                
            } else {
                
                [self.collectionView finishInfiniteScroll];
                [self errorMessage: error];
                
            }
        }];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Shot *shot = self.shots[indexPath.row];
    
    ShotCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"shotCell" forIndexPath:indexPath];

    [cell.image sd_setImageWithURL: [[NSURL alloc] initWithString: shot.loImageURL]];
    
    return cell;
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    Shot *shot = self.shots[self.collectionView.indexPathsForSelectedItems.firstObject.row];
    
    ShotTableViewController *destinationVC = segue.destinationViewController;
    
    if(destinationVC != nil) {
        
        destinationVC.shot = shot;
        
    }
    
}

-(void) errorMessage: (NSString *) error {
    
    if(error != nil) {
    
        _itsOver = YES;
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"There's a problem!" message: error preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController: alert animated: true completion: nil];
        
    }
    
}


@end
