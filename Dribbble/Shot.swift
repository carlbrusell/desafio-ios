//
//  Shot.swift
//  Dribbble
//
//  Created by Carl Osorio on 07/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

import UIKit
import SwiftyJSON

@objc class Shot: NSObject {
    
    @objc let id: String?
    @objc let title: String?
    @objc let descript: String?
    @objc let hiImageURL: String?
    @objc let loImageURL: String?
    @objc let link: String?
    @objc var likes = 0
    @objc var views = 0
    @objc var comments = 0
    @objc var width = 0
    @objc var height = 0
    @objc let tags: [String]?
    @objc let user: User?
    
    init(dic: JSON) {
        
        id = "\(dic["id"].int ?? 0)"
        title = dic["title"].string ?? ""
        
        hiImageURL = dic["images"]["hidpi"].string
        loImageURL = dic["images"]["teaser"].string
        link = dic["html_url"].string ?? ""
        likes = dic["likes_count"].int ?? 0
        views = dic["views_count"].int ?? 0
        comments = dic["comments_count"].int ?? 0
        user = User(dic: dic["user"])
        width = dic["width"].int ?? 0
        height = dic["height"].int ?? 0
        tags = dic["tags"].array?.map({ (dic) -> String in
            return dic.string ?? ""
        })
        
        // remove html from the description
        let description = dic["description"].string
        
        let descriptionNoHTML = description?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        
        descript = descriptionNoHTML
        
    }

}
