//
//  ShotTableViewController.m
//  Dribbble
//
//  Created by Carl Osorio on 08/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

#import "ShotTableViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ShotTableViewController ()

@end

@implementation ShotTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [_authorImage sd_setImageWithURL: [[NSURL alloc] initWithString: _shot.user.avatar]];
    
    [_image sd_setImageWithURL:[[NSURL alloc] initWithString: _shot.hiImageURL]];
    
    _shotTitle.text = _shot.title;
    _author.text = _shot.user.name;
    _date.text = @"ND";
    _views.text = [NSString stringWithFormat:@"%ld", (long)_shot.views];//_shot.views; comments
    _comments.text = [NSString stringWithFormat:@"%ld", (long)_shot.comments];
    _likes.text = [NSString stringWithFormat:@"%ld", (long)_shot.likes];
    _descript.text = _shot.descript;
    
    if(_shot.tags.count == 0) {
        _tags.text = @"No tags.";
    } else {
        _tags.text = [NSString stringWithFormat:@"Tags: %@", [_shot.tags componentsJoinedByString:@", "]];
    }
    
    self.tableView.estimatedRowHeight = 140;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    _imageHeight.constant = _shot.height * self.view.frame.size.width / _shot.width;
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    _imageHeight.constant = _shot.height * size.width / _shot.width;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (IBAction)share:(id)sender {
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems: @[_shot.link] applicationActivities:nil];
    [self presentViewController:activityVC animated:true completion:nil];
    
}

@end
