//
//  DribbbleService.swift
//  Dribbble
//
//  Created by Carl Osorio on 07/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

let token = "595fbad837c545127e07a8f837a870570e6510a4b7118ce952d3dd87f76e556c"

@objc class DribbbleService: NSObject {
    
    public typealias CompletionDribbleAnswer = (_ success: Bool, _ message: String?, _ shots: [Shot]?) -> Void
    
    @objc static func getShots(page: Int, completion: @escaping CompletionDribbleAnswer) {
        
        print("request page \(page)")
        
        Alamofire.request("https://api.dribbble.com/v1/shots?access_token=\(token)&page=\(page)").responseJSON { (response) in
            
            if let data = response.data {
                
                let json = JSON(data: data)
                
                if let array = json.array {
                
                    let shots = array.map({ return Shot(dic: $0) })
                    
                    completion(true, nil, shots)
                    return
                    
                } else {
                    if let errorMessage = json["message"].string {
                        completion(false, errorMessage, nil)
                    }
                }
            }
                
            completion(false, "It was not possible to get new shots.", nil)
            
        }
        
    }

}
