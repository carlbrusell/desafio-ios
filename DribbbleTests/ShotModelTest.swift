//
//  ShotModelTest.swift
//  DribbbleTests
//
//  Created by Carl Osorio on 09/10/17.
//  Copyright © 2017 Carl Brusell. All rights reserved.
//

import XCTest

@testable import Dribbble
import SwiftyJSON

class ShotModelTest: XCTestCase {
    
    var shot: Shot?
    
    override func setUp() {
        super.setUp()
        
        let json = JSON(createMock())
        
        shot = Shot(dic: json)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testModelGeneration() {
        
        XCTAssertEqual(shot?.title, "Illustration for landing page")
        XCTAssertEqual(shot?.width, 400)
        XCTAssertEqual(shot?.height, 300)
        XCTAssertEqual(shot?.views, 6071)
        XCTAssertEqual(shot?.likes, 489)
        XCTAssertEqual(shot?.comments, 14)
        XCTAssertEqual(shot?.link, "https://dribbble.com/shots/3857901-Illustration-for-landing-page")
        XCTAssertEqual(shot?.tags?.count, 9)
        XCTAssertEqual(shot?.descript, "Pudding")
        
        
    }
    
    func createMock() -> [String: Any] {
        
        if let path = Bundle.main.path(forResource: "Shot", ofType: "json") {
            
            let url = URL(fileURLWithPath: path)
            
            if let jsonData = try? Data(contentsOf: url),
                let jsonResult = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: Any] {
                
                return jsonResult ?? [:]
                
            }
            
        }
        return [:]
    }
    
}
